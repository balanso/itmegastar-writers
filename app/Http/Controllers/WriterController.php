<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class WriterController extends Controller
{
    public function show(int $writerId)
    {
        $writer = DB::selectOne(
            "
SELECT w.*, json_agg(b.*) as books
FROM writers w
JOIN books b ON b.author_id = w.id
WHERE w.id=$writerId
GROUP BY w.id"
        );

        if ($writer) {
            $writer->books = json_decode($writer->books);
        }

        return $writer;
    }
}