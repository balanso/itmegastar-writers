<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Writer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Writer::factory(3)
            ->has(Book::factory()->count(3))
            ->create();
    }
}
