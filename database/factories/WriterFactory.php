<?php

namespace Database\Factories;

use App\Models\Writer;
use Illuminate\Database\Eloquent\Factories\Factory;

class WriterFactory extends Factory
{
    protected $model = Writer::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
        ];
    }
}
