# Книги авторов
Сервис возвращает информацию о писателе и его книгах по HTTP-запросу
GET /writers/{writer_id} в JSON
```json
{
    "id": 3,
    "name": "Толстой, Лев",
    "books": [{"id": 1, "name": "Война и мир"}]
}
```

## Настройка сервиса
0. Запустите `composer i`
1. Скопируйте файл .env.example в .env `cp .env.example .env`
2. В файле `.env` укажите настройки базы данных
```dotenv
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=dbname
DB_USERNAME=user
DB_PASSWORD=password
```
3. В корне проекта запустите `php artisan init`, это запустит создание базы данных и тестовых данных
4. Используйте `php artisan serve` для запуска сервера
5. Если всё прошло без ошибок то можно перейти по выданному адресу добавив /writers/{writer_id} и получить данные

